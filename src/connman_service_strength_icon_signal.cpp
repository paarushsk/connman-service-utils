#include <map>
#include <sdbus-c++/Types.h>
#include <sdbus-c++/sdbus-c++.h>
#include <vector>
#include <string>
#include <iostream>
#include <semaphore>

void setCallback(sdbus::ObjectPath objectPath);
void callbackfn(sdbus::Signal& signal);
std::binary_semaphore
	run{0};

void callbackfn(sdbus::Signal& signal)
{
	run.release();
}

void setCallback(sdbus::ObjectPath objectPath)
{
	const char* destinationName = "net.connman";
	const char* interfaceName = "net.connman.Service";
	auto serviceProxy = sdbus::createProxy(destinationName, objectPath);
	serviceProxy->registerSignalHandler(interfaceName, "PropertyChanged", &callbackfn);
	serviceProxy->finishRegistration();
	serviceProxy->unregisterSignalHandler(interfaceName, "PropertyChanged");
}

int main(int argc, char *argv[])
{
	const std::vector<std::string> icons = {"󰤯", "󰤟", "󰤢", "󰤥", "󰤨", "󰤨"};
	const char* destinationName = "net.connman";
	const char* objectPath = "/";
	auto concatenatorProxy = sdbus::createProxy(destinationName, objectPath);
	const char* interfaceName = "net.connman.Manager";

	concatenatorProxy->registerSignalHandler(interfaceName, "ServicesChanged", callbackfn);
	concatenatorProxy->finishRegistration();
	bool flag;
	run.release();

	while(1){
		run.acquire();
		auto method = concatenatorProxy->createMethodCall(interfaceName, "GetServices");
		auto reply = concatenatorProxy->callMethod(method);
		std::vector<sdbus::Struct<sdbus::ObjectPath, std::map<std::string, sdbus::Variant>>> result;
		reply >> result;
		flag=false;
		for (auto i: result){
			std::map<std::string, sdbus::Variant> props = std::get<1>(i);
			if (props.at("State").get<std::string>()=="online" || props.at("State").get<std::string>()=="ready"){
				int s = unsigned(props.at("Strength").get<uint8_t>())/20;
				std::cout << icons[s] << " " << std::endl;
				setCallback(std::get<0>(i));
				flag=true;
				continue;
			}
		}
		if (!flag){
			std::cout << "󰤭 " << std::endl;
		}
	}
	return 0;
}
