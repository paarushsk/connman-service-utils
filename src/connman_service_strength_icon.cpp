#include <map>
#include <sdbus-c++/Types.h>
#include <sdbus-c++/sdbus-c++.h>
#include <vector>
#include <string>
#include <iostream>
#include <unistd.h>

int main(int argc, char *argv[])
{
	const std::vector<std::string> icons = {"󰤯", "󰤟", "󰤢", "󰤥", "󰤨", "󰤨"};
	const char* destinationName = "net.connman";
	const char* objectPath = "/";
	auto concatenatorProxy = sdbus::createProxy(destinationName, objectPath);

	const char* interfaceName = "net.connman.Manager";
	{
		auto method = concatenatorProxy->createMethodCall(interfaceName, "GetServices");
		auto reply = concatenatorProxy->callMethod(method);
		std::vector<sdbus::Struct<sdbus::ObjectPath, std::map<std::string, sdbus::Variant>>> result;
		reply >> result;
		for (auto i: result){
			std::map<std::string, sdbus::Variant> props = std::get<1>(i);
			if (props.at("State").get<std::string>()=="online" || props.at("State").get<std::string>()=="ready"){
				int s = unsigned(props.at("Strength").get<uint8_t>())/20;
				std::cout << icons[s] << " " << std::endl;
				return 0;
			}
		}
		std::cout << "󰤭 " << std::endl;
	}

	return 0;
}
